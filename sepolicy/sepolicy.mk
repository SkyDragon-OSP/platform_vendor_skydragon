#
# This policy configuration will be used by all products that
# inherit from SkyDragon
#

BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/skydragon/sepolicy/common

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += vendor/skydragon/sepolicy/private
BOARD_PLAT_PUBLIC_SEPOLICY_DIR += vendor/skydragon/sepolicy/public

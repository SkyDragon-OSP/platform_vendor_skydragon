# Additional tools
PRODUCT_PACKAGES += \
    e2fsck \
    fsck.exfat \
    lsof \
    mke2fs \
    mount.exfat \
    openvpn \
    tune2fs \
    mkfs.ntfs \
    mount.ntfs \
    fsck.ntfs \
    mkshrc_vendor \
    toybox_vendor \
    sh_vendor \
    vim

# BusyBox
PRODUCT_PACKAGES += \
    busybox

# Charge Animation
PRODUCT_PACKAGES += \
    animation.txt \
    font_charger.png

# CAF Telephony packages
PRODUCT_PACKAGES += \
    ims-ext-common \
    telephony-ext
 PRODUCT_BOOT_JARS += \
    telephony-ext

# Extra packages
PRODUCT_PACKAGES += \
    Lawnchair \
    Longshot \
    OmniJaws \
    OmniStyle \
    Phonograph \
    Stk \
    Terminal

# Fonts
PRODUCT_PACKAGES += \
    SkyDragonFonts

# GCam
PRODUCT_PACKAGES += \
    GCam
PRODUCT_COPY_FILES += \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libAndroidJniUtilsJni.so:system/app/GCam/lib/arm64/libAndroidJniUtilsJni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libGoogleCameraRelease.so:system/app/GCam/lib/arm64/libGoogleCameraRelease.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libImageJni.so:system/app/GCam/lib/arm64/libImageJni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libImageProcJni.so:system/app/GCam/lib/arm64/libImageProcJni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libJniUtilsJni.so:system/app/GCam/lib/arm64/libJniUtilsJni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libPatchLoader.so:system/app/GCam/lib/arm64/libPatchLoader.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libPortraitModeJni.so:system/app/GCam/lib/arm64/libPortraitModeJni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libSeeDarkJni.so:system/app/GCam/lib/arm64/libSeeDarkJni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libadsprpc_app_N.so:system/app/GCam/lib/arm64/libadsprpc_app_N.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libbarhopper.so:system/app/GCam/lib/arm64/libbarhopper.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libcyclops.so:system/app/GCam/lib/arm64/libcyclops.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libeslibm_jni.so:system/app/GCam/lib/arm64/libeslibm_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libeslibw_jni.so:system/app/GCam/lib/arm64/libeslibw_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libentity-clusterer_jni.so:system/app/GCam/lib/arm64/libentity-clusterer_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libfacebeautification.so:system/app/GCam/lib/arm64/libfacebeautification.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libfast_moments_hdr_jni.so:system/app/GCam/lib/arm64/libfast_moments_hdr_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libfilterframework_jni.so:system/app/GCam/lib/arm64/libfilterframework_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libgcam_jni.so:system/app/GCam/lib/arm64/libgcam_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libgyrostabilization-jni.so:system/app/GCam/lib/arm64/libgyrostabilization-jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libhalide_hexagon_host_app.so:system/app/GCam/lib/arm64/libhalide_hexagon_host_app.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libhardwarebuffer-jni.so:system/app/GCam/lib/arm64/libhardwarebuffer-jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libhello-jni.so:system/app/GCam/lib/arm64/libhello-jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libimage.so:system/app/GCam/lib/arm64/libimage.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libimageutilsjni.so:system/app/GCam/lib/arm64/libimageutilsjni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libjni_faceutil.so:system/app/GCam/lib/arm64/libjni_faceutil.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libjni_imgutil.so:system/app/GCam/lib/arm64/libjni_imgutil.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libjni_jpegutil.so:system/app/GCam/lib/arm64/libjni_jpegutil.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libjni_yuvutil.so:system/app/GCam/lib/arm64/libjni_yuvutil.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/liblensoffsetcalculation-jni.so:system/app/GCam/lib/arm64/liblensoffsetcalculation-jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/liblightcycle.so:system/app/GCam/lib/arm64/liblightcycle.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libmicrovideo_tone_map.so:system/app/GCam/lib/arm64/libmicrovideo_tone_map.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libnativehelper_compat_libc++.so:system/app/GCam/lib/arm64/libnativehelper_compat_libc++.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libocr.so:system/app/GCam/lib/arm64/libocr.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/liboliveoil.so:system/app/GCam/lib/arm64/liboliveoil.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libphotobooth_analysis.so:system/app/GCam/lib/arm64/libphotobooth_analysis.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/librectiface_jni.so:system/app/GCam/lib/arm64/librectiface_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/librefocus.so:system/app/GCam/lib/arm64/librefocus.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/librsjni.so:system/app/GCam/lib/arm64/librsjni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libsemanticlift-annotators.so:system/app/GCam/lib/arm64/libsemanticlift-annotators.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libsmartburst-jni.so:system/app/GCam/lib/arm64/libsmartburst-jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libsmartcapture_native.so:system/app/GCam/lib/arm64/libsmartcapture_native.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libtext_orientation_classifier_high_resolution_jni.so:system/app/GCam/lib/arm64/libtext_orientation_classifier_high_resolution_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libtextclassifier_jni_noicu.so:system/app/GCam/lib/arm64/libtextclassifier_jni_noicu.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libtracking-jni-base.so:system/app/GCam/lib/arm64/libtracking-jni-base.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libtracking-jni.so:system/app/GCam/lib/arm64/libtracking-jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libvertical_orientation_classifier_jni_coarse.so:system/app/GCam/lib/arm64/libvertical_orientation_classifier_jni_coarse.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libvision_face_jni.so:system/app/GCam/lib/arm64/libvision_face_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libyuv-jni.so:system/app/GCam/lib/arm64/libyuv-jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libxlib23_jni.so:system/app/GCam/lib/arm64/libxlib23_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libxlib23s_jni.so:system/app/GCam/lib/arm64/libxlib23s_jni.so \
    vendor/skydragon/prebuilt/app/GCam/lib/arm64/libxlib2s_jni.so:system/app/GCam/lib/arm64/libxlib2s_jni.so 

# HDK-Mod
PRODUCT_PACKAGES += \
    HDK-Mod

# Lawnchair
PRODUCT_COPY_FILES += \
    vendor/skydragon/prebuilt/etc/permissions/privapp-permissions-lawnchair.xml:system/etc/permissions/privapp-permissions-lawnchair.xml \
    vendor/skydragon/prebuilt/etc/sysconfig/lawnchair-hiddenapi-package-whitelist.xml:system/etc/sysconfig/lawnchair-hiddenapi-package-whitelist.xml

# Spectrum
#PRODUCT_PACKAGES += \
#    Spectrum

# Google Settings Intelligence
#PRODUCT_PACKAGES += \
#    SettingsIntelligenceGooglePrebuilt

# Turbo
PRODUCT_PACKAGES += \
    Turbo
